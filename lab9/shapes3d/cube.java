package shapes3d;
import shapes2d.Square;
public class cube extends Square{

	public cube(int side) {
		super(side);
		
	}
	
	public int area() {
		return 6 * super.area();
	}
	
	public int volume() {
		return side * super.area();
	}	
	
}