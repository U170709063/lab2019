package shapes3d;
import shapes2d.circle;

public class cylinder extends circle {
	
	int height;
	
	
	public cylinder(int radius, int height) {
		
		super(radius);
		this.height = height;
	}
	
    public double area() {
    	return 2*super.area() + 2*Math.PI*radius * height;
    	
    	
    }
    public String toString() {
    	return"cylinder [height =" +height + super.toString() + "]";
    }
}
